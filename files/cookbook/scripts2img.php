<?php

// gestion du markdown pour les titres en #


Markup('titles','<{$var}',"/^(#*?) /","titles");
function titles($m){
    return str_replace('#','!',$m[1]);
    }
    
// Markup('mditalic','>mdbold',"/\*(.*?)\*/","mditalic");
// function mditalic($m){
//     return "<i>$m[1]</i>";
//     }
//     
// Markup('mdbold','inline',"/\*\*(.*?)\*\*/","mdbold"); // pour que le spuces soient déjà traitées
// function mdbold($m){
//     return "<b>$m[1]</b>";
//     }

function erreur(){
    global $filenameUrl,$type;
    return "<a href=\"".$filenameUrl.".err\">??? $type</a>";
    }

function out(){
    global $code,$out;
    return Keep("<span class='bulle'><span>".prep($code)."</span>$out</span>");
    }

function prepare_code($s) {
    global $KeepToken, $FarmD, $PubDirUrl,$param,$code,$md5,$filename,$filenameUrl,$workdir,$type;
    $code = preg_replace_callback( "/$KeepToken(\\d+?)$KeepToken/", 'cb_expandkpv', $code);
   	$code = preg_replace( "/&gt;/", ">", $code);
   	$code = preg_replace( "/&lt;/", "<", $code);
   	$code = preg_replace( "/\n<:vspace>/",'',$code);
   	$md5 = md5("$param$code");
    if ($param!='')
        $param = ParseArgs($param);
    $workdir="$FarmD/pub/cache";
    $filename = "$FarmD/pub/cache/$s-".$md5;
	$filenameUrl = "$PubDirUrl/cache/$s-".$md5;
	if (!file_exists("$filename.png")){
        echo "producing $type <b>$filename</b><br/>";
        file_put_contents($filename,$code);
        }
    }
    
function prep($code){
    return str_replace(array("\n"," "),array("<br/>","&#160;"),$code);
    }
    
Markup('digraph','directives',"/\(:digraph (.*?)-- (.*?):\)\s*$/","digraph");
function digraph($m) {
	global $param,$code,$filename,$filenameUrl,$workdir,$type,$out;
	$type='Graphviz (dot)';$param = $m[1];$code = $m[2];prepare_code('gv');
    $mapname='';
    if (isset($param['mapname']))
        $mapname=$param['mapname'];
	if (!file_exists("$filename.png"))
        exec("cd $workdir;dot -Tpng $filename -o $filename.png 2> $filename.err;dot -Tcmapx $filename -o $filename.map 2>&1");
    $out="<img border=\"0\" src=\"".$filenameUrl.".png\" alt=\"graphviz\" ";
    if (file_exists($filename.".png")) {
        if (file_exists($filename.".map")) 
            $out.="usemap=\"#$mapname\" ismap=\"ismap\"/>\n".file_get_contents($filename.".map");
        else    
            $out.=" />";
        }
    else
        $out=erreur();
    return out();
    }
    
Markup('neato','directives',"/\(:neato (.*?)-- (.*?):\)\s*$/","neato");
function neato($m) {
	global $param,$code,$filename,$filenameUrl,$workdir,$type,$out;
	$type='Graphviz (neato)';$param = $m[1];$code = $m[2];prepare_code('neato');
	$mapname='';
    if (isset($param['mapname']))
        $mapname=$param['mapname'];
	if (!file_exists("$filename.png"))
        exec("cd $workdir;neato -Tpng $filename -o $filename.png 2> $filename.err;dot -Tcmapx $filename -o $filename.map 2>&1");
    $out="<img border=\"0\" src=\"".$filenameUrl.".png\" alt=\"neato\" ";
    if (file_exists($filename.".png")) {
        if (file_exists($filename.".map")) 
            $out.="usemap=\"#$mapname\" ismap=\"ismap\"/>\n".file_get_contents($filename.".map");
        else    
            $out.=" />";
        }
    else
        $out=erreur();
    return out();
    }
    
Markup('ditaa','directives',"/\(:ditaa (.*?)-- (.*?):\)\s*$/","ditaa");
function ditaa($m) {
	global $param,$code,$filename,$filenameUrl,$workdir,$type,$out;
	$type='Ditaa';$param = $m[1];$code = $m[2];prepare_code('ditaa');
	if (!file_exists("$filename.png")){
        echo "ditaa ";
        file_put_contents($filename,$code);
        exec("cd $workdir;ditaa $filename $filename.png 2> filename.err");
        }
    if (file_exists("$filename.png"))
        $out="<img border=\"0\" src=\"".$filenameUrl.".png\" alt=\"Ditaa\" />";
    else
        $out=erreur();
    return out();
    }

$tex_doc=
'\documentclass[12pt]{standalone} 
\topmargin -15mm
\textheight 24truecm   
\textwidth 16truecm    
\oddsidemargin 5mm
\evensidemargin 5mm   
\setlength\parskip{10pt}
\pagestyle{empty}          
\usepackage{boxedminipage}
\usepackage{amsfonts}
\usepackage{amsmath} 
\usepackage{amssymb}
\usepackage{graphicx}
\usepackage{amsthm}
\usepackage{t1enc}
\usepackage{subfig}
\newtheorem{theorem}{Theorem}[section]
\newtheorem{lemma}[theorem]{Lemma}
\newtheorem{proposition}[theorem]{Proposition}
\newtheorem{corollary}[theorem]{Corollary}
\newtheorem{definition}[theorem]{Definition}
\newtheorem{example}[theorem]{Example}
%s
\begin{document}
%s
\end{document}';

Markup('latex','directives',"/\(:latex (.*?)-- (.*?):\)\s*$/","latex");
function latex($m) {
	global $param,$code,$filename,$filenameUrl,$workdir,$type,$out;
	$type='LaTeX';$param = $m[1];$code = $m[2];prepare_code('texw','png');
	if (!file_exists($filename.".png"))
        exec("cd $workdir;pdflatex -interaction=nonstopmode $filename > $filename.err;pdftoppm $filename.pdf|pnmtopng > $filename.png");// dvipng $filename.dvi"); 
    if (file_exists($filename.".png"))
        $out="<a href='$filenameUrl.pdf'><img style='border:1px solid black;box-shadow: 6px 6px 2px 1px gray;' src=\"".$filenameUrl.".png\" alt=\"LaTeX\"/></a>";
    else 
       $out=erreur();
    return out();
    }
	
Markup('latexf','directives',"/\(:latexf (.*?)-- (.*?):\)/","latexf");
function latexf($m) {
	global $param,$code,$filename,$filenameUrl,$workdir,$tex_doc,$type,$out;
	$type='LaTeXf';$param = $m[1];
	$add_preamble=$param;$code = $m[2];prepare_code('texf');
	if (!file_exists($filename.".png")){ //[TODO regarder comment éviter la surcharge du code
        file_put_contents($filename,sprintf($tex_doc,$add_preamble,$code,)); 
        exec("cd $workdir;latex -interaction=nonstopmode $filename 2> $filename.err;dvipng $filename.dvi -o $filename.png"); 
        }
    if (file_exists($filename.".png"))
        $out="<a href='$filenameUrl.pdf'><img border=\"0\" src=\"".$filenameUrl.".png\" alt=\"Latexf\" /></a>";
    else
        $out=erreur();
    return out();
    }
	
Markup('inlinelatex','>centeredinlinelatex',"/\\$(.*?)\\$/","inlinelatex");
function inlinelatex($m) {
	global $param,$code,$filename,$filenameUrl,$tex_doc,$workdir,$type,$out;
	$type="inlinelatex";$code = $m[1];prepare_code('texi');
	if (!file_exists($filename.".png")){ // [TODO] éviter la surcharge ???
        echo "$filename inlinelatex $code";
        file_put_contents($filename,sprintf($tex_doc,'',"$".$code."$"));
        exec("cd $workdir;latex -interaction=nonstopmode $filename 2> $filename.err;dvipng $filename.dvi -o $filename.png;"); 
        }
    if (file_exists($filename.".png"))
        $out="<img border=\"0\" src=\"".$filenameUrl.".png\" alt=\"inlinelatex\" />";
    else 
        $out=erreur();
    return out();
    }
    
Markup('centeredinlinelatex','inline',"/\\$\\$(.*?)\\$\\$/","<center>\$ $1 \$</center>");

Markup('msc','directives',"/\(:msc (.*?)-- (.*?):\)\s*$/","mscgen");
function mscgen($m) {
	global $param,$code,$filename,$filenameUrl,$tex_doc,$workdir,$out,$type;
    $type="Sequence Diagramme mscgen";$param = $m[1];$code = $m[2];prepare_code('msc');
	if (!file_exists($filename.".png"))
        exec("cd $workdir;mscgen -T png $filename 2> $filename.err"); 
    if (file_exists($filename.".png"))
        $out="<img border=\"0\" src=\"".$filenameUrl.".png\" alt=\"Sequence Diagramme\" />";
    else
        $out=erreur();
    return out();
    }
    
Markup('lilypond','directives',"/\(:lilypond (.*?)-- (.*?):\)\s*$/","lilypond");
function lilypond($m) {
	global $param,$code,$filename,$filenameUrl,$tex_doc,$workdir,$out,$type;
    $type="Lilypond";$param = $m[1];$code = $m[2];prepare_code('ly');
	if (!file_exists($filename.".png"))
        exec("cd $workdir;lilypond $filename 2> $filename.err;pdfcrop --margins '0 0 0 -30' $filename.pdf $filename.temp;pdfcrop $filename.temp $filename;pdftoppm $filename|pnmtopng > $filename.png");
    if (file_exists($filename.".png"))
        $out="<img border=\"0\" src=\"".$filenameUrl.".png\" title='".htmlentities($code)."' alt=\"partition Lilypond\" />";
    else
        $out=erreur();
    return out();
    }

?>
