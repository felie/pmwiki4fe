# Why MyPmWiki ?

A script to install a customized PmWiki with googies (LaTeX, Graphviz, Ditaa, Lilypond...).

![](files/pub/cache/gv-5e59b59f598f1da94e5b46539f6714ba.png)

# Requisite

>sudo apt install graphviz ditaa mscgen texlive-full lilypond imagemagick 

# Installation

Requisite: git for install and apache2 to serve your website
> sudo apt install git apache2

In an empty directory, clone this project
>git clone https://gitlab.adullact.net/felie/mypmwiki.git

And execute the script (no sudo)
>./pmwiki-install <name of the site>

You have to give rights for the webserver (only this time with sudo) 
>sudo chown www-data.www-data -R pmwiki

Move the site where you want - for exemple in /var/www/html by:
(be careful if you still have a pmwiki directory at the destination!)
>sudo mv pmwiki /var/www/html

In this case you'll find the site at http://localhost/pmwiki

TODO
- add minimal gestion of security for newbies
  - blog open or not to others for edition and so on


